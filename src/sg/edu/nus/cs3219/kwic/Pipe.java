package sg.edu.nus.cs3219.kwic;

import java.util.ArrayList;
import java.util.List;

public class Pipe implements IPipe {
	private ArrayList<String> data = new ArrayList<>();
	
	@Override
	public ArrayList<String> read() {
		return this.data;
	}
	
	@Override
	public void write(List<String> data) {
		this.data = new ArrayList<>();
		for (String entry: data) {
			this.data.add(entry);
		}
	}
}
