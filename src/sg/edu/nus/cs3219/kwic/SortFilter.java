package sg.edu.nus.cs3219.kwic;
import java.util.Collections;
import java.util.List;

class SortFilter implements IFilter {
    private IPipe pipe;
    
    public SortFilter(){ }

    @Override
    public void setPipe(IPipe pipe){
        this.pipe = pipe;
    }

    @Override
    public void run(){
    	List<String> data = pipe.read();
    	Collections.sort(data);
        pipe.write(data);
    }
}
