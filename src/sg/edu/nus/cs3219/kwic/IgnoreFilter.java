package sg.edu.nus.cs3219.kwic;
import java.util.ArrayList;
import java.util.HashSet;

class IgnoreFilter implements IFilter {
    private IPipe pipe;
    
    public IgnoreFilter(){ }

    @Override
    public void setPipe(IPipe pipe){
        this.pipe = pipe;
    }

    @Override
    public void run(){
        HashSet<String> ignoreList = new HashSet<String>();
        ArrayList<String> wordList = new ArrayList<String>();
        ArrayList<String> inputList = new ArrayList<String>(pipe.read());
        ArrayList<String> outputList = new ArrayList<String>();
        int i=0;
        for(;i<inputList.size() && !inputList.get(i).equals("") ; i++){
            ignoreList.add(inputList.get(i).toUpperCase());
        }
        for(i = i+1;i<inputList.size(); i++){
            wordList.add(inputList.get(i));
        }

        for(String str : wordList){
            String[] words = str.split("\\s+");
            if(!ignoreList.contains(words[0].toUpperCase())){
                outputList.add(str);
            }
        }
        pipe.write(outputList);
    }
}
