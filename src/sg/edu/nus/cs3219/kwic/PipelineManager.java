package sg.edu.nus.cs3219.kwic;
import java.util.LinkedList;

class PipelineManager {
    private LinkedList<IFilter> filters;
    private Pipe pipe;
    
    public PipelineManager(){ 
        filters = new LinkedList<IFilter>();
        pipe = new Pipe();
    }

    public void addFilter(IFilter filter){
        filters.add(filter);
        filter.setPipe(pipe);
    }
    
    public void run(){
    	
        for(IFilter filter : filters){
            filter.run();
        }
    }
}
