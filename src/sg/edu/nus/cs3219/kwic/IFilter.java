package sg.edu.nus.cs3219.kwic;

public interface IFilter {
	/**
	 * Executes the filter based on the data from its input pipe.
	 */
	void run();
	void setPipe(IPipe pipe);
}
