package sg.edu.nus.cs3219.kwic;

import java.util.List;

public interface IPipe {

	public List<String> read();

	public void write(List<String> data);

}