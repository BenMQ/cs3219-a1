package sg.edu.nus.cs3219.kwic;


public class OutputFilter implements IFilter {
	private IPipe pipe;
	
	@Override
	public void run() {
		for (String line: pipe.read()) {
			System.out.println(line);
		}
	}
	
	@Override
	public void setPipe(IPipe pipe) {
		this.pipe = pipe;
	}
}
