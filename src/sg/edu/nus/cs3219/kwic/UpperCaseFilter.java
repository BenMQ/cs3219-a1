package sg.edu.nus.cs3219.kwic;
import java.util.ArrayList;

class UpperCaseFilter implements IFilter {
    private IPipe pipe;
    
    public UpperCaseFilter(){ }

    @Override
    public void setPipe(IPipe pipe){
        this.pipe = pipe;
    }

    @Override
    public void run(){
        ArrayList<String> inputList = new ArrayList<String>(pipe.read());
        ArrayList<String> outputList = new ArrayList<String>();

        for(String str : inputList){
            StringBuilder sb = new StringBuilder();
            String[] words = str.split("\\s+");
            sb.append(words[0].toUpperCase());
            for(int i=1; i<words.length; i++){
                sb.append(" "+words[i]);
            }
            outputList.add(sb.toString());
        }
        pipe.write(outputList);
    }
}
