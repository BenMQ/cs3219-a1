package sg.edu.nus.cs3219.kwic;

public class KWIC {
	public static void main(String[] args) {
		if (args.length > 0 && args[0].equals("-alt")) {
			try {
				sg.edu.nus.cs3219.kwic.alternative.KeyWordInContext.main(args);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			System.out.println("Enter list of ignore words and list of titles, separated by an empty line.");
			System.out.println("Terminate the input either by ctrl+D or enter \"end\" on a line");
			PipelineManager manager = new PipelineManager();
			manager.addFilter(new InputFilter());
			manager.addFilter(new CircularShiftFilter());
			manager.addFilter(new IgnoreFilter());
			manager.addFilter(new UpperCaseFilter());
			manager.addFilter(new SortFilter());
			manager.addFilter(new OutputFilter());
			
			manager.run();	
		}
	}
}
