package sg.edu.nus.cs3219.kwic.alternative;

public interface IAddItemHandler {

	public String handleAddEntriesManually();

	public String handleAddEntriesFile();

}
