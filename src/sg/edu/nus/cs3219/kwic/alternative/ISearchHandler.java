package sg.edu.nus.cs3219.kwic.alternative;

public interface ISearchHandler {

	public String handleSearch();

}
