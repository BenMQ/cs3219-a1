package sg.edu.nus.cs3219.kwic.alternative;


public interface IAddIgnoreHandler {

	public String handleAddIgnoreManually();

	public String handleAddIgnoreFile();

}
