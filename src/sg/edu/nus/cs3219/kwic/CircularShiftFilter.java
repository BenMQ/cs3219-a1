package sg.edu.nus.cs3219.kwic;

import java.util.ArrayList;

public class CircularShiftFilter implements IFilter {
	private IPipe pipe = null; 
	
	@Override
	public void run() {
		ArrayList<String> original = new ArrayList<String>(this.pipe.read());
		ArrayList<String> rotations = new ArrayList<String>();
		rotations.addAll(original);
		
		int index = 0;
		while (index < original.size() && !original.get(index).trim().equals("")) {
			index ++;
		}
		index ++;
		while(index < original.size()) {
			rotations.addAll(shiftLine(original.get(index)));
			index ++;
		}
		this.pipe.write(rotations);
	}

	@Override
	public void setPipe(IPipe pipe) {
		this.pipe = pipe;
	}
	
	private ArrayList<String> shiftLine(String line) {
		ArrayList<String> results = new ArrayList<String>(); 
		String[] words = line.split("\\s");
		for (int startingIndex = 1; startingIndex < words.length; startingIndex ++) {
			StringBuilder builder = new StringBuilder();
			for (int count = 0; count < words.length; count ++) {
				builder.append(words[(startingIndex + count) % words.length]);
				builder.append(" ");
			}
			results.add(builder.toString().trim());
		}
		return results;
		
	}
	

}
