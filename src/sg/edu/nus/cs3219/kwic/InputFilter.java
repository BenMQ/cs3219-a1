package sg.edu.nus.cs3219.kwic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class InputFilter implements IFilter {
	private IPipe pipe;
	@Override
	public void run() {
		boolean hasEmptyLine = false;
		ArrayList<String> data = new ArrayList<String>();
		try{
			BufferedReader br = new BufferedReader(
					new InputStreamReader(System.in));
			String input;
			while((input = br.readLine()) != null && !input.equals("end")){
				if (input.equals("")) {
					if (!hasEmptyLine) {
						data.add(input.trim());
						hasEmptyLine = true;
					}
				} else {
					data.add(input.trim());
				}
			}
		} catch(IOException io) {
			io.printStackTrace();
		}	
		if (!hasEmptyLine) {
			data.add(0, "");
		}
		pipe.write(data);
	}

	@Override
	public void setPipe(IPipe pipe) {
		this.pipe = pipe;
		
	}
}
